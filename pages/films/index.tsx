import Head from 'next/head';
import { FC } from 'react';
import Film from '../../app/components/Film';

import Header from '../../app/components/Header';
import styles from '../../styles/Home.module.scss';

import { IFilms } from '../../types/types';

interface FilmsTypeProps {
  films: IFilms;
}

const Films: FC<FilmsTypeProps> = ({ films }) => {
  console.log(films);

  return (
    <div>
      <Head>
        <title>Films</title>
      </Head>

      <Header />

      <div className={styles.container}>
        <h1 className="text-4xl my-5">Фильмы</h1>
        <div className="grid grid-cols-5 gap-x-10">
          {films &&
            films.map((film) => {
              return <Film key={film.id} {...film} />;
            })}
        </div>
      </div>
    </div>
  );
};

export default Films;

export const getStaticProps = async () => {
  const response = await fetch(
    'https://api.themoviedb.org/3/movie/popular?api_key=48d98328f96c9cb98768fa9d5e2ccca6',
  );

  const data = await response.json();

  return {
    props: {
      films: data.results,
    },
  };
};
