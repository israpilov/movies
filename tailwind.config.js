const primary = '#0d253f';
const background = '#100e19';

module.exports = {
  content: ['./pages/**/*.{js,ts,jsx,tsx}', './app/components/**/*.{js,ts,jsx,tsx}'],
  theme: {
    extend: {    
      colors: {
      primary,
      background,
    },},
  },
  plugins: [],
};
