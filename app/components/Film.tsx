import Image from 'next/image';
import Link from 'next/link';
import React from 'react';

import { IFilms } from '../../types/types';

type FilmsTypeProps = {
  film: IFilms;
};

const Film = ({ film }) => {
  return (
    <div>
      <Link href="/" className="block">
        <a>
          <div>
            <Image src="/" width={177} height={303} alt="Film" />
          </div>
          <div>King’s Man: Начало</div>
        </a>
      </Link>
    </div>
  );
};

export default Film;
