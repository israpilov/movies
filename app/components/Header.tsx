import Image from 'next/image';
import Link from 'next/link';
import { FC } from 'react';

import styles from '../../styles/Home.module.scss';
import logoSvg from '../assets/image/logo.svg';
import searchSvg from '../assets/image/search.svg';

const Header: FC = () => {
  return (
    <header className="py-5 bg-primary ">
      <div className={styles.container}>
        <div className="flex gap-x-5 justify-between items-center">
          <div className="flex gap-x-5">
            <Link href="/">
              <a>
                <Image src={logoSvg} width={154} height={20} alt="logo" />
              </a>
            </Link>
            <ul className="flex gap-x-5 text-white">
              <li>
                <Link href="/films">
                  <a>Фильмы</a>
                </Link>
              </li>
              <li>
                <Link href="/">
                  <a>Сериалы</a>
                </Link>
              </li>
            </ul>
          </div>
          <Link href="/">
            <a>
              <Image src={searchSvg} width={20} height={20} alt="logo" />
            </a>
          </Link>
        </div>
      </div>
    </header>
  );
};

export default Header;
